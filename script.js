(function () {

    'use strict';

    var scene;
    var camera;
    var renderer;
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;

    var mouse = null;

    var textureLoader = new THREE.TextureLoader();
    var raycaster = new THREE.Raycaster();

    var updaters = [];

    var POSTER_RATIO = 1.449;
    var NUMBER_POSTERS = 10;
    var posterImageCounter = 1;
    var circle = -Math.PI / 2;

    var intersectedObject = null;
    var diff;
    var inertia;
    var inertiaDirection;

    init();

    function init() {

        initScene();
        initCamera(windowWidth, windowHeight);
        initRenderer(windowWidth, windowHeight);
        initContent();
        initUpdater(scene, camera);

        function initScene() {
            scene = new THREE.Scene();
            scene.fog = new THREE.Fog(0x1b051d, 10, 20);
        }

        function initCamera(width, height) {
            camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
            camera.position.z = 10;
            //camera.rotation.x = -0.15;
        }

        function initRenderer(width, height) {
            renderer = new THREE.WebGLRenderer({
                alpha: true
            });
            renderer.setSize(width, height);
            document.body.appendChild(renderer.domElement);
        }

        function initUpdater(scene, camera) {
            function draw() {
                requestAnimationFrame(draw);
                update();
                renderer.render(scene, camera);
            }
            draw();
        }

    }

    function initContent() {

        makeLight(scene);

        initPoster(-6);
        initPoster(-5);
        initPoster(-4);
        initPoster(-3);
        initPoster(-2);
        initPoster(-1);
        initPoster(0);
        initPoster(1);
        initPoster(2);
        initPoster(3);
        initPoster(4);
        initPoster(5);
        initPoster(6);

        function initPoster(position) {
            var circlePosition = map(position, -8, 8, 0, 2 * Math.PI) + circle;

            var planeGeometry = new THREE.PlaneGeometry(3, 3 * POSTER_RATIO, 1);
            var planeMaterial = new THREE.MeshLambertMaterial({
                map: textureLoader.load('poster-' + (posterImageCounter++) + '.jpg'),
                side: THREE.FrontSide
            });
            if (posterImageCounter === 6) {
                posterImageCounter = 1;
            }
            var plane = new THREE.Mesh(planeGeometry, planeMaterial);

            updaters.push(function updater() {

                circlePosition = map(position, -8, 8, 0, 2 * Math.PI) + circle;

                setCoordinates(plane, circlePosition);
                if (circlePosition >= 2 * Math.PI) {
                    circlePosition = 0;
                }
            });

            scene.add(plane);
        }

        function setCoordinates(plane, circlePosition) {
            plane.position.x = Math.cos(circlePosition) * 8;
            plane.position.z = (Math.sin(circlePosition) - 0.5) * 8;
        }

    }

    function makeLight(scene) {
        var light = new THREE.SpotLight(0xffffff);
        light.position.x = 10;
        light.position.y = 50;
        light.position.z = 130;
        scene.add(light);
    }

    function findClicks() {
        if (!mouse) {
            return;
        }

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(scene.children);

        if (intersects.length > 0) {
            if (intersectedObject !== intersects[0].object) {
                if (intersectedObject !== null) {
                    intersectedObject.material.emissive.setHex(intersectedObject.currentHex);
                }
                intersectedObject = intersects[0].object;
                intersectedObject.material.emissive.setHex( 0xff0000 );
            }
        } else {
            if (intersectedObject) {
                intersectedObject.material.emissive.setHex(intersectedObject.currentHex);
            }
            intersectedObject = null;
        }

        mouse = null;
    }

    function update() {
        findClicks();

        if (diff) {
            circle = circle + map(diff, -1000, 1000, -Math.PI, Math.PI);
        } else if (inertia) {
            circle = circle + map(inertia, -1000, 1000, -Math.PI, Math.PI);
            inertia = inertiaSub(inertia, inertiaDirection);
            if (inertiaDirection > 0 && inertia < 0) {
                inertia = null;
            } else if (inertiaDirection < 0 && inertia > 0) {
                inertia = null;
            }
        }
        updaters.forEach(function (updater) {
            updater(circle);
        });

        diff = null;

    }

    function inertiaSub(current, direction) {
        var newValue = current - direction * Math.exp(Math.abs(current * 0.001));
        return newValue;
    }

    document.addEventListener('touchstart', onTouchStart, false);

    function onTouchStart(event) {

        var prev = null;

        event.preventDefault();
        mouse = {
            x: (event.touches[0].clientX / window.innerWidth ) * 2 - 1,
            y: -(event.touches[0].clientY / window.innerHeight ) * 2 + 1
        };

        prev = event.touches[0].clientX;

        var onTouchMoveBound = onTouchMove.bind(null, event.touches[0].clientX);

        document.addEventListener('touchmove', onTouchMoveBound, false);
        document.addEventListener('touchend', onTouchEnd, false);

        function onTouchEnd(event) {
            diff = 0;
            document.removeEventListener('touchmove', onTouchMoveBound, false);
            document.removeEventListener('touchend', onTouchEnd, false);
        }

        function onTouchMove(start, event) {
            var x = event.touches[0].clientX;
            diff = (prev - x) / 1000 * windowWidth;
            prev = x;

            inertia = diff;
            inertiaDirection = diff > 0 ? 1 : -1;
        }
    }

    function map(x, in_min, in_max, out_min, out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

}());
